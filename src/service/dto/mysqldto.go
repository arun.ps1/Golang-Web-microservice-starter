package dto

type Channel_slack_settings struct {
	Integration_name   string
	Channel_id         string
	Slack_sender_name  string
	Slack_Webhook_url  string
	Slack_channel_name string
	Message            string
	Tags               string
	Tenant_id          string
}
